(function () {
    angular.module('app.config', [])
        .constant('AppConfig', {
            "api": {
                "endpoint": "http://spa-ebs.dev"
            }
        });
}());
