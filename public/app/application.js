(function() {
    angular.module('app', [
        'ngResource',
        'checklist-model',
        'app.config',
        'ui.router',
        'app.main',
        'app.category',
        'app.product'
    ])
    .config(routes);

    routes.$inject = ['$stateProvider', '$urlRouterProvider'];
    function routes($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/index");
        $stateProvider
            .state('index', {
                url: "/index?category?page",
                params: {
                    page: {
                        value: '1'
                    }
                },
                templateUrl: 'app/components/main/main.html',
                controller: 'MainController',
                controllerAs: 'vm'
            })
            .state('createProduct', {
                url: "/product",
                templateUrl: 'app/components/product/product-create.html',
                controller: 'ProductCreateController',
                controllerAs: 'vm'
            })
            .state('editProduct', {
                url: "/product/{productId}",
                templateUrl: 'app/components/product/product-edit.html',
                controller: 'ProductEditController',
                controllerAs: 'vm'
            })
            .state('createCategory', {
                url: "/category",
                templateUrl: 'app/components/category/category-create.html',
                controller: 'CategoryCreateController',
                controllerAs: 'vm'
            })
            .state('editCategory', {
                url: "/category/{categoryId}",
                templateUrl: 'app/components/category/category-edit.html',
                controller: 'CategoryEditController',
                controllerAs: 'vm'
            })
            .state('categories', {
                url: "/index?page",
                params: {
                    page: {
                        value: '1',
                        squash: true
                    }
                },
                templateUrl: 'app/components/home/home.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            })
            .state('notFound', {
                url: "/not-found",
                templateUrl: 'app/components/not-found/not-found.html',
                controller: 'NotFoundController'
            });

    }

}());
