(function() {
    angular.module('app')
        .directive('headerDirective', headerDirective);
    
    function headerDirective() {
        return {
            templateUrl: 'app/shared/header/header.html'
        }
    }
}());
