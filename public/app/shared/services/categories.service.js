(function() {
    angular.module('app')
        .service('CategoriesService', CategoriesService);

    CategoriesService.$inject = ['AppConfig', '$resource'];
    function CategoriesService(AppConfig, $resource) {
        this.category = {};
        this.categories = {};

        var resourceRoute = [
            AppConfig.api.endpoint,
            'categories',
            ':categoryId',
            ':state'
        ].join('/');
        var options = {
            categoryId: '@categoryId',
            state: '@state',
            page: '@page'
        };
        this.resource = $resource(resourceRoute, options, {update: {method: 'PUT'}});
        this.getById = getById;
        this.store = store;
        this.update = update;
    }

    function getById(id) {
        this.category = {};
        var successCallback = function(response) {
            this.category = response.data;
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .get({categoryId: id, state: 'edit'})
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

    function store() {
        var successCallback = function(response) {
            this.category = response.data;
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .save(this.category)
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

    function update() {
        var successCallback = function(response) {
            this.category = response.data;
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .update({categoryId: this.category.id}, this.category)
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }
}());
