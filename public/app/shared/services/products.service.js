(function() {
    angular.module('app')
        .service('ProductsService', ProductsService);

    ProductsService.$inject = ['AppConfig', '$resource'];
    function ProductsService(AppConfig, $resource) {
        this.product = {};
        this.products = {};
        this.categories = {};
        this.pagination = [];

        var resourceRoute = [
            AppConfig.api.endpoint,
            'products',
            ':productId',
            ':state',
            ':category'
        ].join('/');
        var options = {
            productId: '@productId',
            state: '@state',
            category: '@category',
            page: '@page'
        };
        this.resource = $resource(resourceRoute, options, {update: {method: 'PUT'}});
        this.getList = getList;
        this.getById = getById;
        this.store = store;
        this.update = update;
    }

    function getList(stateParams) {
        var params = {
            page: stateParams.page
        };
        if (stateParams.category) {
            params.state = 'category';
            params.category = stateParams.category;
        }

        var successCallback = function(response) {
            this.products = response.data.products.data;
            this.categories = response.data.categories;
            this.pagination = [];
            if (response.data.products.last_page > 1) {
                for (var i = 1; i <= response.data.products.last_page; i++) {
                    (function(i) {
                        this.pagination.push(i);
                    }.bind(this)(i));
                }
            }
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .get(params)
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

    function getById(id) {
        this.product = {};
        var successCallback = function(response) {
            var categories = angular.copy(response.data.product.categories);
            this.product = response.data.product;
            this.categories = response.data.categories;
            this.product.categories = [];
            for (var i = 0; i < categories.length; i++) {
                this.product.categories.push(categories[i].id);
            }
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .get({productId: id, state: 'edit'})
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

    function store() {
        var successCallback = function(response) {
            this.product = response.data;
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .save(this.product)
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

    function update() {
        var successCallback = function(response) {
            this.product = response.data;
        };
        var errorCallback = function(error) {

        };
        var promise = this.resource
            .update({productId: this.product.id}, this.product)
            .$promise;

        promise.then(successCallback.bind(this), errorCallback);
        return promise;
    }

}());
