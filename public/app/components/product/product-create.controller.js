(function() {
    angular.module('app.product')
        .controller('ProductCreateController', ProductCreateController);

    ProductCreateController.$inject = ['$state', 'ProductsService'];
    function ProductCreateController($state, ProductsService) {
        this.productsService = ProductsService;
        this.createProduct = createProduct;
        (function() {
            this.productsService.product = {};
        }.bind(this)());

        function createProduct() {
            ProductsService.store()
                .then(function() {
                    $state.go('index');
                })
        }
    }
}());
