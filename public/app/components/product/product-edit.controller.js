(function() {
    angular.module('app.product')
        .controller('ProductEditController', ProductEditController);

    ProductEditController.$inject = ['$state', '$stateParams', 'ProductsService'];
    function ProductEditController($state, $stateParams, ProductsService) {
        this.productsService = ProductsService;
        this.editProduct = editProduct;
        (function() {
            ProductsService.getById($stateParams.productId);
        }());

        function editProduct() {
            ProductsService.update()
                .then(function() {
                    $state.go('index');
                })
        }
    }
}());
