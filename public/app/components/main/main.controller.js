(function() {
    angular.module('app.main')
        .controller('MainController', MainController);

    MainController.$inject = ['$stateParams', 'ProductsService'];
    function MainController($stateParams, ProductsService) {
        this.productsService = ProductsService;
        (function() {
            ProductsService.getList($stateParams);
        }());
    }
}());
