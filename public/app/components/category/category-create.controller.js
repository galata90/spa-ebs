(function() {
    angular.module('app.category')
        .controller('CategoryCreateController', CategoryCreateController);

    CategoryCreateController.$inject = ['$state', 'CategoriesService'];
    function CategoryCreateController($state, CategoriesService) {
        this.categoriesService = CategoriesService;
        this.createCategory = createCategory;
        (function() {
            this.categoriesService.category = {};
        }.bind(this)());

        function createCategory() {
            CategoriesService.store()
                .then(function() {
                    $state.go('index');
                })
        }
    }
}());
