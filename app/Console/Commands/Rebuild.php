<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Rebuild extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:rebuild {--update}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild the application   ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->confirm('Are you sure you want to run the service? It will erase all the data you have.');
        $updateRepos = $this->option('update');
        if ($updateRepos) {
            $this->info('Updating composer dependencies');
            exec("composer update");
            exec("composer dump-autoload -o");
        }
        $this->info('Running the manager...');
        $this->call('clear-compiled');
        $this->call('optimize');
        $this->runMigrationsAndSeeds();
        $this->call('cache:clear');
        $this->call('route:clear');
        $this->call('config:clear');
        $this->call('config:cache');
        chdir("public");
        $this->info('Getting frontend dependencies');
        exec("bower install");
        $this->info('The application has been rebuilt. Enjoy.');
    }

    public function runMigrationsAndSeeds()
    {
        $this->call('migrate:refresh');
        $this->call('db:seed');
    }
}
