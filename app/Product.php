<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = ['name', 'price'];

    public $timestamps = false;

    public static $rules = [
        'name'  => 'required|min:1|max:100',
        'price' => 'required'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_products', 'product_id', 'category_id');
    }

}