<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Validator;

class ProductsController extends Controller
{

    protected $product;
    protected $category;

    public function __construct(Product $product, Category $category)
    {
        $this->product  = $product;
        $this->category = $category;
    }

    public function index(Request $request)
    {
        $categories = $this->category->get();
        $products   = $this->product->paginate(10);
        $data       = [
            'data' => [
                'categories' => $categories,
                'products'   => $products
            ]
        ];

        return response()->json($data);
    }

    public function getByCategory($id)
    {
        $categories = $this->category->all();
        $products   = $this->product->whereHas('categories', function ($query) use ($id) {
            $query->where('categories.id', $id);
        })
        ->paginate(10);

        $data       = [
            'data' => [
                'categories' => $categories,
                'products'   => $products
            ]
        ];

        return response()->json($data);
    }

    public function edit($id)
    {
        $categories = $this->category->all();
        $product    = $this->product
            ->with('categories')
            ->find($id);
        if ($product) {
            return response()->json([
                'data' => [
                    'categories' => $categories,
                    'product'    => $product
                ]

            ]);
        }

        return response()->json([
            'error' => 'Record not found'
        ], 404);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), Product::$rules);
        if ($validator->passes()) {
            $product = $this->product->find($id);
            if ($product) {
                $product->name  = $request->get('name');
                $product->price = $request->get('price');
                $product->save();

                if ($request->has('categories')) {
                    $product->categories()->sync($request->get('categories'));
                }

                return response()->json([
                    'data' => $product
                ]);
            }

            return response()->json([
                'error' => 'Record not found'
            ], 500);
        }

        return response()->json([
            'error' => $validator
        ], 302);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Product::$rules);
        if ($validator->passes()) {
            $product        = new Product();
            $product->name  = $request->get('name');
            $product->price = $request->get('price');
            $product->save();

            if ($request->has('categories')) {
                $product->categories()->sync($request->get('categories'));
            }

            return response()->json([
                'data' => $product
            ]);
        }

        return response()->json([
            'error' => $validator
        ], 302);
    }

}
