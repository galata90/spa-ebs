<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoriesController extends Controller
{

    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function edit($id)
    {
        $category = $this->category->find($id);

        if ($category) {
            return response()->json([
                'data' => $category
            ]);
        }

        return response()->json([
            'error' => 'Record not found'
        ], 404);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => "required|unique:categories,name,{$id}|min:1|max:100",
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $category = $this->category->find($id);
            if ($category) {
                $category->name = $request->get('name');
                $category->save();

                return response()->json([
                    'data' => $category
                ]);
            }

            return response()->json([
                'error' => 'Record not found'
            ], 500);
        }

        return response()->json([
            'error' => $validator
        ], 302);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Category::$rules);
        if ($validator->passes()) {
            $category        = new Category();
            $category->name  = $request->get('name');
            $category->save();

            return response()->json([
                'data' => $category
            ]);
        }

        return response()->json([
            'error' => $validator
        ], 302);
    }

}
