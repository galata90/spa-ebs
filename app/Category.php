<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name'];

    protected $appends = ['products_count'];

    public $timestamps = false;

    public static $rules = [
        'name' => 'required|unique:categories,name|min:1|max:100',
    ];

    public function products()
    {
        return $this->belongsToMany('App\Product', 'categories_products', 'category_id', 'product_id');
    }

    public function getProductsCountAttribute()
    {
        return $this->products()->count();
    }

}