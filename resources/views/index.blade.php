<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="libs/bootstrap/dist/css/bootstrap.min.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- External libs -->
    <script src="libs/jquery/dist/jquery.min.js"></script>
    <!-- Angular core -->
    <script src="libs/angular/angular.js"></script>
    <!-- Angular libs -->
    <script src="libs/angular-resource/angular-resource.js"></script>
    <!-- Application -->
    <script src="app/application.js"></script>
    <script src="app/config.js"></script>
    <!-- Angular libs-->
    <script src="libs/checklist-model/checklist-model.js"></script>
    <script src="libs/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="libs/angular-flash-alert/dist/angular-flash.min.js"></script>
    <!-- Application Modules-->
    <script src="app/components/main/main.js"></script>
    <script src="app/components/product/product.js"></script>
    <script src="app/components/category/category.js"></script>
    <!-- Application Controllers-->
    <script src="app/components/main/main.controller.js"></script>
    <script src="app/components/product/product-create.controller.js"></script>
    <script src="app/components/product/product-edit.controller.js"></script>
    <script src="app/components/category/category-create.controller.js"></script>
    <script src="app/components/category/category-edit.controller.js"></script>
    <!-- Application Services -->
    <script src="app/shared/services/categories.service.js"></script>
    <script src="app/shared/services/products.service.js"></script>
    <script src="app/shared/services/error-handler.service.js"></script>
    <!-- Application Directives -->
    <script src="app/shared/header/header.directive.js"></script>

</head>
<body>
<div ng-app="app">
    <header-directive></header-directive>
    <div class="container-fluid">
        <section ui-view></section>
    </div>
    <footer-directive></footer-directive>
    <div class="flash-messages">
        <flash-message duration="5000"></flash-message>
    </div>
</div>
</body>
</html>
