<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker      = Faker::create();
        $records    = 20;
        $products   = [];

        for($i = 0; $i < $records; $i++) {
            $products[] = [
                'name'  => ucfirst($faker->words($nb = 3, $asText = true)),
                'price' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100)
            ];
        }

        \DB::table('products')->insert($products);
    }
}
