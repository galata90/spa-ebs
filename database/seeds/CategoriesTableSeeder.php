<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker      = Faker::create();
        $records    = 10;
        $categories = [];

        for($i = 0; $i < $records; $i++) {
            $categories[] = [
                'name' => ucfirst($faker->unique()->word)
            ];
        }

        \DB::table('categories')->insert($categories);
    }
}
