<?php

use Illuminate\Database\Seeder;

class CategoriesProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $values                  = [];
        $uniqueCategories        = [];
        $maxCategoriesPerProduct = 5;
        $categories              = 10; //same as categories
        $products                = 20; //same as products

        for ($i = 1; $i <= $products; $i++) {
            $categoriesForProduct = rand(1, $maxCategoriesPerProduct);
            for ($j = 1; $j <= $categoriesForProduct; $j++) {
                $categoryID = rand(1, $categories);
                if (!in_array($categoryID, $uniqueCategories)) {
                    $uniqueCategories[] = $categoryID;
                    $values[]           = [
                        'category_id' => $categoryID,
                        'product_id' => $i
                    ];
                }
            }
            $uniqueCategories = [];
        }

        DB::table('categories_products')->insert($values);
    }
}
