<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_products', function($table)
        {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->integer('product_id')->unsigned()->index();

            $table->foreign('category_id', 'categories_products_category_id_foreign')
                ->references('id')
                ->on('categories')
                ->onDelete('cascade');
            $table->foreign('product_id', 'categories_products_product_id_foreign')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_products');
    }
}
